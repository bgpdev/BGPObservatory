# Application

The structure of the application is composed of multiple parts.

## The Provider Package
This package contains multiple modules that are being used for populating the database.
It is separated between BGP related data sources which provide for example BGP Update messages or BGP RIBs and meta which contains data sources that provide meta information such as the RIPE WHOIS service or the Geolite DB.

## The Core Package
This package contains general functionality that can be used by all other packages. It contains an interface to the database and the configuration of the whole system, as well as a generic Network interface such that we can track our outgoing requests when populating the database.

## The Analyzer Package
This package contains code that analyzes and builds upon the data stored in the database.
