#
# This Dockerfile is used to build the final production image.
# It contains the BGPObservatory that validates BGPUpdate messages.
#

#
# The build stage which builds the application.
#

FROM gradle:alpine AS gradle

# Set the working directory to /srv/observatory/
WORKDIR /srv/observatory/

# Set the current user of the image to 'root'
USER root

# Copy over the project to the Dockerfile
COPY . /srv/observatory/

# Create a java executable by running installDist
RUN gradle installDist

#
# The final stage of the Multistage Docker image.
# By using multistage builds we wipe everything from before clean.
#

FROM openjdk:alpine

# Set the working directory again.
WORKDIR /srv/observatory/

# Copy over the resources and the binary executable.
COPY --from=gradle /srv/observatory/build/install/observatory/ /srv/observatory/

# Run the BGPObservatory program.
CMD ["/srv/observatory/bin/observatory"]
