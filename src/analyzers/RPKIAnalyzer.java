package analyzers;

import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import collections.trees.BinaryTree;
import collections.trees.Node;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import core.BGPReport;
import core.Core;
import core.Network;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import irr.RPKI;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;
import network.entities.Prefix;
import po.PO;
import utility.Matcher;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Map;

public class RPKIAnalyzer extends Analyzer
{
    private final boolean RIPE_API = false;
    private final String endpoint = "http://tradeseer.nl:23456/api/bgp/validity?";

    private BinaryTree<ArrayList<RPKI.ROA>> rpsl;

    @Override
    public JsonObject call() throws Exception
    {
        /* Profile this code */
        long profile = Core.profiler.start();

        JsonObject report = new JsonObject();
        report.add("prefixes", new JsonObject());

        // Retrieve the AS_PATH attribute of the BGP Update.
        AS_PATH path = BGPUtility.getAS_PATH(target.update);
        if (path == null)
            return report;

        long origin = BGPUtility.getOriginatingAS(path);

        /* ------------------------------------------------------------------------
         * For every prefix, generate a report.
         * ------------------------------------------------------------------------*/

        for (Prefix prefix : target.update.nlri)
        {
            JsonObject prefix_obj = new JsonObject();

            if(RIPE_API)
                validateRIPE(origin, prefix, prefix_obj);
            else
                validateLocal(origin, prefix, prefix_obj);

            report.get("prefixes").getAsJsonObject().add(prefix.toString(), prefix_obj);
        }

        /* Profile this code */
        Core.profiler.stop(profile, "RPKIAnalyzer");

        return report;
    }

    private void validateRIPE(Long origin, Prefix prefix, JsonObject prefix_obj) throws Exception
    {
        URL url = new URL("https://stat.ripe.net/data/rpki-validation/data.json?sourceapp=bgptudelft01&resource=" + origin + "&prefix=" + prefix.toString());
        JsonReader reader = new JsonReader(new InputStreamReader(url.openStream()));
        JsonParser parser = new JsonParser();

        JsonObject response = parser.parse(reader).getAsJsonObject();
        String status = response.get("data").getAsJsonObject().get("status").getAsString();

        switch(status)
        {
            case "NotFound":
                prefix_obj.addProperty("rpki-status", "nonexistent");
                break;
            case "Valid":
                prefix_obj.addProperty("rpki-status", "valid");
                break;
            case "Invalid":
                prefix_obj.addProperty("rpki-status", "invalid");
                break;
            default:
                prefix_obj.addProperty("rpki-status", "Error during lookup");
        }
    }

    private void validateLocal(Long origin, Prefix prefix, JsonObject prefix_obj) throws Exception
    {
        String prefix_string = prefix.toString();
        Integer length = Integer.parseInt(prefix_string.split("/")[1]);

        // Check for exact matches.
        IPAddress address = new IPAddressString(prefix.toString()).toAddress();
        BitSet set = BitSet.valueOf(address.getBytes());
        Node<ArrayList<RPKI.ROA>> node = rpsl.get(set, address.getPrefixLength());

        while(node != null)
        {
            if(node.value != null)
            {
                ArrayList<RPKI.ROA> roas = node.value;
                for(RPKI.ROA roa : roas)
                {
                    if(roa.origin.equals(origin) && roa.maxLength >= length)
                    {
                        prefix_obj.addProperty("rpki-status", "valid");
                        return;
                    }
                    else
                    {
                        prefix_obj.addProperty("rpki-status", "invalid");
                        return;
                    }
                }
            }

            node = node.parent;
        }

        prefix_obj.addProperty("rpki-status", "nonexistent");
    }

    @Override
    public void setTarget(BGPReport report)
    {
        super.setTarget(report);
        rpsl = Core.getRPKI(target.record.header.timestamp);
    }

    private void validate(Long origin, Prefix prefix, JsonObject prefix_obj)
    {

    }

    /**
     * When used in a Chain, this method may be used as a pass / fail method.
     * @param indicators The JsonObject as a result of this analyzer.
     * @return True if it does not generate a match on RIPE RPKI data call.
     */
    public static boolean pass(JsonObject indicators)
    {
        return !indicators.get("rpki-status").equals("valid");
    }
}
