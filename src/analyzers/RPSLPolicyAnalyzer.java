package analyzers;

import bgp.utility.BGPUtility;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import core.BGPReport;
import core.Core;
import irr.IRR;
import network.entities.Prefix;
import rpsl.RPSLPolicy;
import rpsl.expression.filter.FilterMatch;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class RPSLPolicyAnalyzer extends Analyzer
{
    private IRR registry;

    @Override
    public void setTarget(BGPReport report)
    {
        super.setTarget(report);
        registry = Core.getIRR(target.record.header.timestamp);
    }

    /**
     * Analyze if each prefix of this BGPUpdate message passes the RPSL Filters.
     * @return A report containing route-leakage related information of this BGPUpdate.
     */
    @Override
    public JsonObject call()
    {
        /* Profile this code */
        long profile = Core.profiler.start();

        JsonObject response = new JsonObject();
        response.add("prefixes", new JsonObject());

        // Retrieve the AS_PATH attribute of the BGP Update.
        // If it is not present the BGPUpdate message only contains withdrawn routes.
        List<Long> path = BGPUtility.getReversePath(target.update);
        if (path == null)
            return response;

        /* -------------------------------------------------------------
         * Detecting using RPSL Routing Policies.
         * -------------------------------------------------------------*/
        for (Prefix prefix : target.update.nlri)
        {
            JsonObject prefix_obj = new JsonObject();
            JsonObject filter = new JsonObject();
            prefix_obj.add("filters", filter);

            // Create an object for the first AS.
            filter.add("AS" + path.get(0).toString(), new JsonObject());

            for (int i = 0; i < path.size() - 1; i++)
            {
                long AS = path.get(i);
                long next = path.get(i + 1);

                filter.add("AS" + next, new JsonObject());

                /*
                 * Check the IMPORT policies of the 'next' AS.
                 */
                {
                    // Check if the IMPORT Policies match
                    Set<RPSLPolicy> policies = registry.getImportPolicies(next);
                    if (!policies.isEmpty())
                    {
                        filter.get("AS" + next).getAsJsonObject().addProperty("import", "NO_FILTER_FOR_PEER");
                        for (RPSLPolicy policy : policies)
                            if (policy.hasPeer(AS))
                            {
                                FilterMatch match = policy.accept(prefix.toString(), "AS" + AS);

                                String current = filter.get("AS" + next).getAsJsonObject().get("import").getAsString();
                                if(current.equals("NO_FILTER_FOR_PEER") || match != FilterMatch.REJECT)
                                {
                                    filter.get("AS" + next).getAsJsonObject().addProperty("import", match.toString());
                                    filter.get("AS" + next).getAsJsonObject().addProperty("import-policy", policy.getPolicy());
                                }
                            }
                    }
                    else
                        filter.get("AS" + next).getAsJsonObject().addProperty("import", "NO_FILTERS");
                }

                /*
                 * Check the EXPORT policies of the 'AS' AS.
                 */
                {
                    // Check if the EXPORT Policies match
                    Set<RPSLPolicy> policies = registry.getExportPolicies(AS);
                    if (!policies.isEmpty())
                    {
                        filter.get("AS" + AS).getAsJsonObject().addProperty("export", "NO_FILTER_FOR_PEER");
                        for (RPSLPolicy policy : policies)
                            if (policy.hasPeer(next))
                            {
                                FilterMatch match = policy.accept(prefix.toString(), "AS" + next);

                                String current = filter.get("AS" + AS).getAsJsonObject().get("export").getAsString();
                                if(current.equals("NO_FILTER_FOR_PEER") || match != FilterMatch.REJECT)
                                {
                                    filter.get("AS" + AS).getAsJsonObject().addProperty("export", match.toString());
                                    filter.get("AS" + AS).getAsJsonObject().addProperty("export-policy", policy.getPolicy());
                                }
                            }
                    } else
                        filter.get("AS" + AS).getAsJsonObject().addProperty("export", "NO_FILTERS");
                }
            }
            response.get("prefixes").getAsJsonObject().add(prefix.toString(), prefix_obj);
        }

        /* Profile this code */
        Core.profiler.stop(profile, "RPSLPolicyAnalyzer");

        return response;
    }

    /**
     * When used in a Chain, this method may be used as a pass / fail method.
     * @param indicators The JsonObject as a result of this analyzer.
     * @return True if it is classified as a Route Leak according to ASRank.
     */
    public static boolean pass(JsonObject indicators)
    {
        for(Map.Entry<String, JsonElement> x : indicators.get("filters").getAsJsonObject().entrySet())
        {
            JsonObject filter = x.getValue().getAsJsonObject();
            if(filter.has("export"))
            {
                String policy = x.getValue().getAsJsonObject().get("export").getAsString();
                if (policy.equals("REJECT") || policy.equals("NO_FILTERS") || policy.equals("NO_FILTER_FOR_PEER"))
                    return true;
            }

            if(filter.has("import"))
            {
                String policy = x.getValue().getAsJsonObject().get("import").getAsString();
                if (policy.equals("REJECT") || policy.equals("NO_FILTERS") || policy.equals("NO_FILTER_FOR_PEER"))
                    return true;
            }
        }

        return false;
    }
}