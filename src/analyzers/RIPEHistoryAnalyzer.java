package analyzers;

import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import core.Core;
import network.entities.Prefix;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;

public class RIPEHistoryAnalyzer extends Analyzer
{
    @Override
    public JsonObject call() throws Exception
    {
        /* Profile this code */
        long profile = Core.profiler.start();

        JsonObject report = new JsonObject();
        report.add("prefixes", new JsonObject());

        // Retrieve the AS_PATH attribute of the BGP Update.
        AS_PATH path = BGPUtility.getAS_PATH(target.update);
        if (path == null)
            return report;

        long origin = BGPUtility.getOriginatingAS(path);

        /* ------------------------------------------------------------------------
         * For every prefix, generate a report.
         * ------------------------------------------------------------------------*/

        for (Prefix prefix : target.update.nlri)
        {
            JsonObject prefix_obj = new JsonObject();

            try
            {
                URL url = new URL("https://stat.ripe.net/data/routing-history/data.json?sourceapp=bgptudelft01&resource=" + prefix.toString() +
                        "&min_peers=1&endtime=" + target.record.header.timestamp.getEpochSecond());

                JsonReader reader = new JsonReader(new InputStreamReader(url.openStream()));
                JsonParser parser = new JsonParser();

                JsonObject response = parser.parse(reader).getAsJsonObject();
                JsonArray array = response.get("data").getAsJsonObject().get("by_origin").getAsJsonArray();

                prefix_obj.addProperty("announced-earlier (exact)", false);
                prefix_obj.addProperty("announced-earlier (parent)", false);

                for(JsonElement element : array)
                {
                    JsonObject AS = element.getAsJsonObject();
                    if(AS.get("origin").getAsString().equals(Long.toString(origin)))
                    {
                        for(JsonElement resource : AS.get("prefixes").getAsJsonArray())
                        {
                            JsonObject res = resource.getAsJsonObject();
                            for(JsonElement timeline : res.get("timelines").getAsJsonArray())
                            {
                                JsonObject line = timeline.getAsJsonObject();

                                Instant start = Instant.parse(line.get("starttime").getAsString() + "Z");
                                Instant end = Instant.parse(line.get("endtime").getAsString() + "Z");
                                Duration duration = Duration.between(start, end);

                                if(res.get("prefix").getAsString().equals(prefix.toString()))
                                {
                                    prefix_obj.addProperty("announced-earlier (exact)", true);

                                    long period = duration.getSeconds();
                                    if(prefix_obj.has("announced-duration (exact)"))
                                        period += prefix_obj.get("announced-duration (exact)").getAsLong();

                                    prefix_obj.addProperty("announced-duration (exact)", period);
                                }
                                else
                                {
                                    prefix_obj.addProperty("announced-earlier (parent)", true);

                                    long period = duration.getSeconds();
                                    if(prefix_obj.has("announced-duration (parent)"))
                                        period += prefix_obj.get("announced-duration (parent)").getAsLong();

                                    prefix_obj.addProperty("announced-duration (parent)", period);
                                }
                            }
                        }
                    }
                }
            }
            catch(IOException e)
            {
                prefix_obj.addProperty("announced-earlier (exact)", false);
                prefix_obj.addProperty("announced-earlier (parent)", false);
                prefix_obj.addProperty("ripe-history-network-error", true);
            }

            report.get("prefixes").getAsJsonObject().add(prefix.toString(), prefix_obj);
        }

        /* Profile this code */
        Core.profiler.stop(profile, "RIPEHistoryAnalyzer");

        return report;
    }

    /**
     * When used in a Chain, this method may be used as a pass / fail method.
     * @param indicators The JsonObject as a result of this analyzer.
     * @return Always true
     */
    public static boolean pass(JsonObject indicators)
    {
        return true;
    }
}
