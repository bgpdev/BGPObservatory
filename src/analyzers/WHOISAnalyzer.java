package analyzers;

import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import collections.trees.BinaryTree;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import core.BGPReport;
import core.Core;
import core.Network;
import network.entities.Prefix;
import po.PO;
import utility.Matcher;

import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Map;

public class WHOISAnalyzer extends Analyzer
{
    private Network network;
    private BinaryTree<ArrayList<PO>> registry_tree;
    /**
     * Retrieve the WHOIS RPSL data for this
     * @return The report that has been made by this analyzers.
     */
    @Override
    public JsonObject call()
    {
        /* Profile this code */
        long profile = Core.profiler.start();

        JsonObject response = new JsonObject();
        response.add("prefixes", new JsonObject());

        try
        {
            // Retrieve the AS_PATH attribute of the BGP Update.
            AS_PATH path = BGPUtility.getAS_PATH(target.update);
            if(path == null)
                return response;

            /* ------------------------------------------------------------------------
             * For every prefix, generate a report.
             * ------------------------------------------------------------------------*/

            for(Prefix prefix : target.update.nlri)
            {
                JsonObject prefix_report = new JsonObject();
                String prefix_string = prefix.toString();

                /*
                 * Classify the prefix.
                 */

                Matcher.Classification v = Matcher.match(prefix_string,
                        target.update.pathattributes,
                        registry_tree,
                        network);

                if(!v.equals(Matcher.Classification.NONE))
                    prefix_report.addProperty("whois-match", v.toString());

                // Add the prefix object to the prefix list.
                response.get("prefixes").getAsJsonObject().add(prefix_string, prefix_report);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);

        }

        /* Profile this code */
        Core.profiler.stop(profile, "WHOISAnalyzer");

        return response;
    }

    @Override
    public void setTarget(BGPReport report)
    {
        super.setTarget(report);
        network = Core.getNetwork(target.record.header.timestamp);
        registry_tree = Core.getRegistryTree(target.record.header.timestamp);
    }

    /**
     * When used in a Chain, this method may be used as a pass / fail method.
     * @param indicators The JsonObject as a result of this analyzer.
     * @return True if it does not generate a match on any IRR / WHOIS registry
     */
    public static boolean pass(JsonObject indicators)
    {
        return !indicators.has("whois-match");
    }
}

