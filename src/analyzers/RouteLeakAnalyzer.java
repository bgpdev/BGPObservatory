package analyzers;

import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import core.BGPReport;
import core.Core;
import core.Network;
import core.Relationship;
import network.entities.Prefix;
import providers.RoutingInformationBase;
import utility.Triple;

import java.util.*;

/**
 * The RouteLeakAnalyzer class analyzes the BGPUpdate messages and verifies whether a route leak has occurred or not.
 */
public class RouteLeakAnalyzer extends Analyzer
{
    private class Relation
    {
        public final long from;
        public final long to;
        public final Relationship.Type type;

        private Relation(long from, long to, Relationship.Type type)
        {
            this.from = from;
            this.to = to;
            this.type = type;
        }
    }

    private Network network;

    @Override
    public void setTarget(BGPReport report)
    {
        super.setTarget(report);
        network = Core.getNetwork(target.record.header.timestamp);
    }

    /**
     * Analyze if this BGPUpdate contains any route leaks.
     * To detect this we use multiple datasources such as:
     * - ASRank
     * - RPSL Policies
     * - [O] PeeringDB
     * @return A report containing route-leakage related information of this BGPUpdate.
     */
    @Override
    public JsonObject call()
    {
        /* Profile this code */
        long profile = Core.profiler.start();

        JsonObject report = new JsonObject();
        report.add("prefixes", new JsonObject());

        // Retrieve the AS_PATH attribute of the BGP Update.
        // If it is not present a route is withdrawn.
        List<Long> path = BGPUtility.getReversePath(target.update);
        if(path == null)
            return report;

        /* --------------------------------------------------
         * Create earlier seen triples from RIB entries.
         * --------------------------------------------------*/

        // Get the Triples of this path.
        Collection<Triple<Integer, Integer, Integer>> triples = BGPUtility.getTriples(BGPUtility.getAS_PATH(target.update));

        for(Prefix prefix_obj : target.update.nlri)
        {
            String prefix = prefix_obj.toString();

            JsonObject properties = new JsonObject();
            report.get("prefixes").getAsJsonObject().add(prefix, properties);

            Collection<RoutingInformationBase.BGPEntry> entries = target.collector.rib.getPrefix(prefix);
            if(entries.isEmpty())
                for (Triple<Integer, Integer, Integer> triple : triples)
                    detect(properties, triple);

            for (RoutingInformationBase.BGPEntry entry : entries)
            {
                AS_PATH shadow_path = entry.getAS_PATH();
                Collection<Triple<Integer, Integer, Integer>> shadow = BGPUtility.getTriples(shadow_path);

                properties.addProperty("old-path", BGPUtility.toString(shadow_path));

                if (entry.peer == target.record.peerASN)
                    for (Triple<Integer, Integer, Integer> triple : triples)
                        if (!shadow.contains(triple))
                        {
//                            System.out.println(prefix + ": Shadow Triples: ");
//                            for(Triple T : shadow)
//                                System.out.println("(" + T.getFirst() + ", " + T.getSecond() + ", " + T.getThird() + ")");
//
//                            System.out.println(prefix + ": New Triple: ");
//                            System.out.println("(" + triple.getFirst() + ", " + triple.getSecond() + ", " + triple.getThird() + ")");

                            detect(properties, triple);
                        }
            }
        }

        /* Profile this code */
        Core.profiler.stop(profile, "RouteLeakAnalyzer");

        return report;
    }

    private void detect(JsonObject properties, Triple<Integer, Integer, Integer> triple)
    {
        Relation x = new Relation(triple.getThird(), triple.getSecond(), network.getRelation(triple.getThird(), triple.getSecond()));
        Relation y = new Relation(triple.getSecond(), triple.getFirst(), network.getRelation(triple.getSecond(), triple.getFirst()));

        // Provider - Customer - Provider Leak
        if(x.type == Relationship.Type.PROVIDER_CUSTOMER && y.type == Relationship.Type.CUSTOMER_PROVIDER)
        {
            JsonObject leak = new JsonObject();
            leak.addProperty("from", x.from);
            leak.addProperty("offender", x.to);
            leak.addProperty("to", y.to);
            leak.addProperty("type", "Provider-Customer-Provider");
            if(!properties.has("asrank-route-leaks"))
                properties.add("asrank-route-leaks", new JsonArray());
            properties.get("asrank-route-leaks").getAsJsonArray().add(leak);
        }

        // Peer - Peer - Peer Leak
        if(x.type == Relationship.Type.PEER_PEER && y.type == Relationship.Type.PEER_PEER)
        {
            // Exclude T1 from Peer to Peer to Peer Leaks.
            boolean T1 = false;
            for(Relationship r : network.getRelations(x.to))
            {
                if(r.type == Relationship.Type.CUSTOMER_PROVIDER)
                {
                    T1 = true;
                    break;
                }
            }

            if(!T1)
            {
                JsonObject leak = new JsonObject();
                leak.addProperty("from", x.from);
                leak.addProperty("offender", x.to);
                leak.addProperty("to", y.to);
                leak.addProperty("type", "Peer-Peer-Peer");

                if(!properties.has("asrank-route-leaks"))
                    properties.add("asrank-route-leaks", new JsonArray());
                properties.get("asrank-route-leaks").getAsJsonArray().add(leak);
            }
        }

        // Peer - Peer - Provider Leak
        if(x.type == Relationship.Type.PEER_PEER && y.type == Relationship.Type.CUSTOMER_PROVIDER)
        {
            JsonObject leak = new JsonObject();
            leak.addProperty("from", x.from);
            leak.addProperty("offender", x.to);
            leak.addProperty("to", y.to);
            leak.addProperty("type", "Peer-Peer-Provider");

            if(!properties.has("asrank-route-leaks"))
                properties.add("asrank-route-leaks", new JsonArray());
            properties.get("asrank-route-leaks").getAsJsonArray().add(leak);
        }

        // Provider - Customer/Peer - Peer Leak
        if(x.type == Relationship.Type.PROVIDER_CUSTOMER && y.type == Relationship.Type.PEER_PEER)
        {
            JsonObject leak = new JsonObject();
            leak.addProperty("from", x.from);
            leak.addProperty("offender", x.to);
            leak.addProperty("to", y.to);
            leak.addProperty("type", "Provider-Peer-Peer");

            if(!properties.has("asrank-route-leaks"))
                properties.add("asrank-route-leaks", new JsonArray());
            properties.get("asrank-route-leaks").getAsJsonArray().add(leak);
        }
    }

    /**
     * When used in a Chain, this method may be used as a pass / fail method.
     * @param indicators The JsonObject as a result of this analyzer.
     * @return True if it is classified as a Route Leak according to ASRank.
     */
    public static boolean pass(JsonObject indicators)
    {
        return indicators.has("asrank-route-leaks");
    }
}