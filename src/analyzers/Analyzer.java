package analyzers;

import com.google.gson.JsonObject;
import core.BGPReport;

import java.util.concurrent.Callable;

/**
 * An Analyzer receives as update a BGP
 */
public abstract class Analyzer implements Callable<JsonObject>
{
    // The BGPReport that contains the message that should be analyzed.
    protected BGPReport target;

    public void setTarget(BGPReport report)
    {
        this.target = report;
    }
}
