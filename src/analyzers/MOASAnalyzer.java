package analyzers;

import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import com.google.gson.JsonObject;
import core.Core;
import network.entities.Prefix;
import providers.RoutingInformationBase;

import java.util.Collection;

public class MOASAnalyzer extends Analyzer
{
    @Override
    public JsonObject call()
    {
        /* Profile this code */
        long profile = Core.profiler.start();

        JsonObject report = new JsonObject();
        report.add("prefixes", new JsonObject());

        // Retrieve the AS_PATH attribute of the BGP Update.
        AS_PATH path = BGPUtility.getAS_PATH(target.update);
        if(path == null)
            return report;

        // Retrieve the Origin of the AS_PATH
        long origin = BGPUtility.getOriginatingAS(path);

        for (Prefix prefix : target.update.nlri)
        {
            String prefix_str = prefix.toString();
            JsonObject prefix_report = new JsonObject();

            prefix_report.addProperty("introduces-soas", false);
            prefix_report.addProperty("introduces-moas", true);

           Collection<RoutingInformationBase.BGPEntry> entries = target.collector.rib.getPrefix(prefix_str);

            if(entries.isEmpty())
            {
                prefix_report.addProperty("introduces-soas", true);
                prefix_report.addProperty("introduces-moas", false);
            }

            for (RoutingInformationBase.BGPEntry entry : entries)
            {
                AS_PATH entry_path = entry.getAS_PATH();
                long entry_origin = BGPUtility.getOriginatingAS(entry_path);
                if(entry_origin == origin)
                    prefix_report.addProperty("introduces-moas", false);
            }

            report.get("prefixes").getAsJsonObject().add(prefix_str, prefix_report);
        }

        /* Profile this code */
        Core.profiler.stop(profile, "MOASAnalyzer");

        return report;
    }

    /**
     * When used in a Chain, this method may be used as a pass / fail method.
     * @param indicators The JsonObject as a result of this analyzer.
     * @return True if it introduces a MOAS or SOAS.
     */
    public static boolean pass(JsonObject indicators)
    {
        return indicators.get("introduces-soas").getAsBoolean() || indicators.get("introduces-moas").getAsBoolean();
    }
}
