package analyzers;

import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import core.BGPReport;
import core.Core;
import core.Network;
import network.entities.Prefix;
import po.PO;
import rpsl.RPSLObject;
import utility.Matcher;
import whois.resolver.generic.GenericResolver;

import java.util.ArrayList;
import java.util.Map;

/**
 * Temporary class to query to LACNIC, should be changed to raw registry DUMP.
 */
public class LACNICAnalyzer extends Analyzer
{
    private Network network;

    @Override
    public JsonObject call() throws Exception
    {
        /* Profile this code */
        long profile = Core.profiler.start();

        JsonObject response = new JsonObject();
        response.add("prefixes", new JsonObject());

        try
        {
            // Retrieve the AS_PATH attribute of the BGP Update.
            AS_PATH path = BGPUtility.getAS_PATH(target.update);
            if(path == null)
                return response;

            /* ------------------------------------------------------------------------
             * For every prefix, generate a report.
             * ------------------------------------------------------------------------*/

            for(Prefix prefix : target.update.nlri)
            {
                JsonObject prefix_report = new JsonObject();
                String prefix_string = prefix.toString();

                GenericResolver resolver = new GenericResolver("whois.lacnic.net");
                ArrayList<RPSLObject> results = resolver.query(prefix.toString());

                ArrayList<PO> route_objects = new ArrayList<>();
                for(RPSLObject object : results)
                    if(object.hasProperty("inetnum") && object.hasProperty("aut-num") && object.getProperty("aut-num").matches("AS[0-9]+"))
                    {
                        PO route = new PO();
                        route.prefix = object.getProperty("inetnum");
                        route.origin = Long.parseLong(object.getProperty("aut-num").replace("AS", ""));
                        route_objects.add(route);
                    }

                Matcher.Classification v = Matcher.match(target.update.pathattributes, route_objects, network);

                if(!v.equals(Matcher.Classification.NONE))
                    prefix_report.addProperty("whois-match", v.toString());

                // Add the prefix object to the prefix list.
                response.get("prefixes").getAsJsonObject().add(prefix_string, prefix_report);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }

        /* Profile this code */
        Core.profiler.stop(profile, "LACNICAnalyzer");

        return response;
    }

    @Override
    public void setTarget(BGPReport report)
    {
        super.setTarget(report);
        network = Core.getNetwork(target.record.header.timestamp);
    }

    public static boolean pass(JsonObject indicators)
    {
        return !indicators.has("whois-match");

    }
}
