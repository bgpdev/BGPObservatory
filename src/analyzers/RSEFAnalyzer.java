package analyzers;

import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import core.Core;
import inet.ipaddr.IPAddress;
import inet.ipaddr.IPAddressString;
import network.entities.Prefix;
import po.PO;
import rsef.RSEFRecord;
import collections.trees.BinaryTree;
import collections.trees.Node;

import java.time.Instant;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

public class RSEFAnalyzer extends Analyzer
{
    private static BinaryTree<String> prefixes_v4 = new BinaryTree<>();
    private static BinaryTree<String> prefixes_v6 = new BinaryTree<>();
    private static HashMap<Long, String> asns = new HashMap<>();

    private static void initialize(Instant time)
    {
        try
        {
            for (RSEFRecord record : Core.getRSEF(time))
            {
                // Insert IPv4 prefixes into the BinaryTree
                if (record.type.equals("ipv4") && record.id != null)
                {
                    int prefix_length = 32 - ((int) (Math.log(record.value) / Math.log(2)));
                    IPAddress address = new IPAddressString(record.start + "/" + prefix_length).toAddress();
                    prefixes_v4.insert(BitSet.valueOf(address.getBytes()), prefix_length, record.id);
                }

                // Insert IPv4 prefixes into the BinaryTree
                if (record.type.equals("ipv6") && record.id != null)
                {
                    IPAddress address = new IPAddressString(record.start + "/" + record.value).toAddress();
                    prefixes_v6.insert(BitSet.valueOf(address.getBytes()), record.value, record.id);
                }

                if (record.type.equals("asn") && record.id != null)
                {
                    long start = Long.parseLong(record.start);
                    for (long i = start; i < start + record.value; i++)
                        asns.put(i, record.id);
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Retrieve the WHOIS RSEF data for this
     * @return The report that has been made by this analyzers.
     */
    @Override
    public JsonObject call()
    {
        if(asns.isEmpty())
            initialize(target.record.header.timestamp);

        /* Profile this code */
        long profile = Core.profiler.start();

        JsonObject response = new JsonObject();
        response.add("prefixes", new JsonObject());

        try
        {
            // Retrieve the AS_PATH attribute of the BGP Update.
            AS_PATH path = BGPUtility.getAS_PATH(target.update);
            if(path == null)
                return response;

            long origin = BGPUtility.getOriginatingAS(path);

            /* ------------------------------------------------------------------------
             * For every prefix, generate a report.
             * ------------------------------------------------------------------------*/

            for(Prefix prefix : target.update.nlri)
            {
                JsonObject prefix_report = new JsonObject();
                prefix_report.addProperty("same-resource-holder (prefix-origin)", false);
                prefix_report.addProperty("same-resource-holder (RO-origin)", false);

                /*
                 * Indicator: [same-umbrella-organization]
                 **/

                BitSet set = BitSet.valueOf(prefix.prefix);

                if(prefix.type.equals(Prefix.Type.IPv4))
                {
                    Node<String> x = prefixes_v4.get(set, prefix.length);

                    // Since prefixes listed in RSEF do not have a parent we only have to check the first match.
                    if(x != null && x.value != null && asns.containsKey(origin) && x.value.equals(asns.get(origin)))
                        prefix_report.addProperty("same-resource-holder (prefix-origin)", true);
                }
                else
                {
                    Node<String> x = prefixes_v6.get(set, prefix.length);

                    // Since prefixes listed in RSEF do not have a parent we only have to check the first match.
                    if(x != null && x.value != null && asns.containsKey(origin) && x.value.equals(asns.get(origin)))
                        prefix_report.addProperty("same-resource-holder (prefix-origin)", true);
                }

                /*
                 * Check if there is any relation between the registered route object and any organization.
                 */
                Node<ArrayList<PO>> x = Core.getRegistryTree(target.record.header.timestamp).get(set, prefix.length);
                while(x != null && !prefix_report.get("same-resource-holder (RO-origin)").getAsBoolean())
                {
                    if(x.value != null)
                        for(PO route : x.value)
                        {
                            String id1 = asns.get(route.origin);
                            String id2 = asns.get(origin);
                            if(id1 != null && id2 != null && id1.equals(id2))
                                prefix_report.addProperty("same-resource-holder (RO-origin)", true);
                        }
                    x = x.parent;
                }

                response.get("prefixes").getAsJsonObject().add(prefix.toString(), prefix_report);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);

        }

        /* Profile this code */
        Core.profiler.stop(profile, "RSEFAnalyzer");

        return response;
    }

    /**
     * When used in a Chain, this method may be used as a pass / fail method.
     * @param indicators The JsonObject as a result of this analyzer.
     * @return True if it does not generate a match on any IRR / WHOIS registry
     */
    public static boolean pass(JsonObject indicators)
    {
        boolean ROO = indicators.get("same-resource-holder (RO-origin)").getAsBoolean();
        boolean PO = indicators.get("same-resource-holder (prefix-origin)").getAsBoolean();
        return !ROO & !PO;
    }
}
