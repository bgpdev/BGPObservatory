package analyzers;

import com.google.gson.JsonObject;
import core.Core;
import core.Relationship;
import utility.Triple;

import java.util.HashMap;

/**
 * The RIPENeighbourAnalyzer makes a prediction on
 */
public class RIPENeighbourAnalyzer extends Analyzer
{
    private static HashMap<String, Relationship.Type> relationships;

    @Override
    public JsonObject call() throws Exception
    {
        /* Profile this code */
        long profile = Core.profiler.start();

        JsonObject report = new JsonObject();
        report.add("prefixes", new JsonObject());

        /**
         *
         */
        Triple<Integer, Integer, Integer> triple = null;

        /* ------------------------------------------------------------------------
         * For every prefix, generate a report.
         * ------------------------------------------------------------------------*/

//        for (Prefix prefix : target.update.nlri)
//        {
//            JsonObject prefix_obj = new JsonObject();
//
//            URL url = new URL("https://stat.ripe.net/data/routing-history/data.json?resource=" + prefix.toString() +
//                    "&normalise_visibility=true&min_peers=1&endtime=" + target.record.header.timestamp.getEpochSecond());
//
//            JsonReader reader = new JsonReader(new InputStreamReader(url.openStream()));
//            JsonParser parser = new JsonParser();
//
//            JsonObject response = parser.parse(reader).getAsJsonObject();
//            JsonArray array = response.get("data").getAsJsonObject().get("by_origin").getAsJsonArray();
//
//            prefix_obj.addProperty("announced-earlier (exact)", false);
//            prefix_obj.addProperty("announced-earlier (parent)", false);
//
//            for(JsonElement element : array)
//            {
//                JsonObject AS = element.getAsJsonObject();
//                if(AS.get("origin").getAsString().equals(Long.toString(origin)))
//                {
//                    for(JsonElement resource : AS.get("prefixes").getAsJsonArray())
//                    {
//                        JsonObject res = resource.getAsJsonObject();
//                        for(JsonElement timeline : res.get("timelines").getAsJsonArray())
//                        {
//                            JsonObject line = timeline.getAsJsonObject();
//
//                            Instant start = Instant.parse(line.get("starttime").getAsString() + "Z");
//                            Instant end = Instant.parse(line.get("endtime").getAsString() + "Z");
//                            Duration duration = Duration.between(start, end);
//
//                            if(line.get("visibility").getAsDouble() >= VISIBILITY_THRESHOLD)
//                            {
//                                if(res.get("prefix").getAsString().equals(prefix.toString()))
//                                {
//                                    prefix_obj.addProperty("announced-earlier (exact)", true);
//
//                                    long period = duration.getSeconds();
//                                    if(prefix_obj.has("announced-duration (exact)"))
//                                        period += prefix_obj.get("announced-duration (exact)").getAsLong();
//
//                                    prefix_obj.addProperty("announced-duration (exact)", period);
//                                }
//                                else
//                                {
//                                    prefix_obj.addProperty("announced-earlier (parent)", true);
//
//                                    long period = duration.getSeconds();
//                                    if(prefix_obj.has("announced-duration (parent)"))
//                                        period += prefix_obj.get("announced-duration (parent)").getAsLong();
//
//                                    prefix_obj.addProperty("announced-duration (parent)", period);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//
//            report.get("prefixes").getAsJsonObject().add(prefix.toString(), prefix_obj);
//        }

        /* Profile this code */
        Core.profiler.stop(profile, "RIPENeighbourAnalyzer");

        return report;
    }

    /**
     * When used in a Chain, this method may be used as a pass / fail method.
     * @param indicators The JsonObject as a result of this analyzer.
     * @return Always true
     */
    public static boolean pass(JsonObject indicators)
    {
        return true;
    }
}
