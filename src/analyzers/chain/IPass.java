package analyzers.chain;

import com.google.gson.JsonObject;

public interface IPass
{
    boolean pass(JsonObject indicators);
}
