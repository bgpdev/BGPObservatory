package analyzers.chain;

import analyzers.Analyzer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import network.entities.Prefix;
import system.threads.ThreadEngine;
import utility.JSONMerger;

import java.util.*;

public class ChainedAnalyzer extends Analyzer
{
    private static class Filter
    {
        final Analyzer analyzer;
        final IPass validator;

        private Filter(Analyzer analyzer, IPass validator)
        {
            this.analyzer = analyzer;
            this.validator = validator;
        }
    }

    private List<Filter> filters = new ArrayList<>();

    public void add(Analyzer analyzer, IPass validator)
    {
        this.filters.add(new Filter(analyzer, validator));
    }

    @Override
    public JsonObject call() throws Exception
    {
        JsonObject indicators = new JsonObject();

        // Create a shallow copy of the NLRI
        ArrayList<Prefix> nlri = new ArrayList<>(target.update.nlri);

        for(Filter filter : filters)
        {
            if(target.update.nlri.isEmpty())
                break;

            filter.analyzer.setTarget(target);
            JsonObject result = filter.analyzer.call();

            // Merge the reports together. TODO: Room for Improvement.
            JSONMerger.extend(indicators, JSONMerger.ConflictStrategy.PREFER_SECOND_OBJ, result);

            /* --------------------------------------
             * Validate per prefix
             * --------------------------------------*/
            Iterator<Prefix> i = target.update.nlri.iterator();
            while (i.hasNext())
            {
                String prefix = i.next().toString();
                JsonObject properties = indicators.get("prefixes").getAsJsonObject().get(prefix).getAsJsonObject();

                if (!filter.validator.pass(properties))
                    i.remove();
            }
        }

        target.update.nlri = nlri;
        return indicators;
    }
}
