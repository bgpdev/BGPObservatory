package system.threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class that holds, executes all concurrent tasks.
 */
public class ThreadEngine
{
    // The singleton ThreadEngine
    private static ThreadEngine engine = null;

    // The size of the thread pool
    private final int THREAD_POOL_SIZE = 30;

    // The queue in which requests are being entered.
    private ExecutorService pool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

    private ThreadEngine(){}

    public static ExecutorService getPool()
    {
        if(engine == null)
            engine = new ThreadEngine();

        return engine.pool;
    }
}
