package providers;

import bgp.BGPReader;
import bgp.PathAttribute;
import bgp.attributes.AS_PATH;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Routing Information Base
 * An instance of this class holds the Routing Information Base (RIB) information of a particular collector.
 * It holds all routes previously advertised.
 */
public class RoutingInformationBase
{
    /** The static HashMap that holds all routes currently in the RIB. */
    private static Map<String, Collection<BGPEntry>> entries = new HashMap<>();

    public static long total = 0;
    public static long counter = 0;

    /** Each prefix may contain multiple BGP entries */
    public class BGPEntry
    {
        /** The peer from which this route was received */
        public final long peer;

        /** The timestamp at which this route was received */
        public final long timestamp;

        /** The BGP Attributes associated with this prefix and peer, stored as binary to enhance boot performance. */
        private byte[] buffer;

        /** The parsed PathAttributes of this BGP Message */
        public AS_PATH path = null;

        /**
         * Constructs a BGPEntry from an RIB_AFI entry.
         * @param entry The RIB_ENTRY used to construct the BGPEntry.
         */
        public BGPEntry(RIB_AFI.RIB_ENTRY entry)
        {
            this.peer = entry.peer.ASN;
            this.timestamp = entry.originatedtime;
            this.buffer = entry.attributes;

            total += entry.attributes.length;
            counter++;
        }

        public BGPEntry(long peer, long timestamp, Map<String, PathAttribute> attributes)
        {
            this.peer = peer;
            this.timestamp = timestamp;
            this.buffer = null;
            this.path = (AS_PATH)attributes.get("AS_PATH");
        }

        public AS_PATH getAS_PATH()
        {
            if(path == null)
                load();
            return path;
        }

        public void load()
        {
            if(buffer != null)
            {
                try
                {
                    DataInputStream stream = new DataInputStream(new ByteArrayInputStream(buffer));
                    while (stream.available() > 0)
                    {
                        BGPReader.Settings settings = new BGPReader.Settings();
                        settings.EXTENDED_AS4 = true;
                        PathAttribute a = PathAttribute.fromStream(stream, settings);

                        // TODO: Currently only store AS_PATH to save memory.
                        if(a.getClass().getSimpleName().toUpperCase().equals("AS_PATH"))
                            path = (AS_PATH) a;
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    System.exit(-1);
                }

                buffer = null;
            }
        }
    }

    /**
     * Inserts a RIB_AFI entry into the Routing Information Base.
     * @param entry The RIB_AFI entry that should be inserted.
     * @throws Exception
     */
    public synchronized void insert(RIB_AFI entry)
    {
        // Retrieve the textual representation of the prefix.
        String textual = entry.prefix.toString();

        if(!entries.containsKey(textual))
            entries.put(textual, new ConcurrentLinkedQueue<>());

        Collection<BGPEntry> list = entries.get(textual);

        // For each path of this prefix, add it to the list.
        for(RIB_AFI.RIB_ENTRY x : entry.entries)
            list.add(new BGPEntry(x));
    }

    public synchronized void update(String prefix, long peer, long timestamp, Map<String, PathAttribute> attributes)
    {
        if(!entries.containsKey(prefix))
            entries.put(prefix, new ConcurrentLinkedQueue<>());

        for (final Iterator<BGPEntry> i = entries.get(prefix).iterator(); i.hasNext();)
        {
            final BGPEntry element = i.next();
            if(element.peer == peer)
            {
                i.remove();
                break;
            }
        }

        entries.get(prefix).add(new BGPEntry(peer, timestamp, attributes));
    }

    public synchronized void withdrawn(String prefix, long peer)
    {
        if(!entries.containsKey(prefix))
            return;

        for (final Iterator<BGPEntry> i = entries.get(prefix).iterator(); i.hasNext();)
        {
            final BGPEntry element = i.next();
            if(element.peer == peer)
            {
                i.remove();
                return;
            }
        }
    }

    public synchronized Collection<BGPEntry> getPrefix(String prefix)
    {
        if(!entries.containsKey(prefix))
            return new ConcurrentLinkedQueue<>();
        else
            return entries.get(prefix);
    }
}
