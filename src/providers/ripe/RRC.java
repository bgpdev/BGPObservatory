package providers.ripe;

import bgp.BGPMessage;
import bgp.BGPReader;
import bgp.BGPUpdate;
import core.BGPReport;
import mrt.MRTReader;
import mrt.MRTRecord;
import mrt.type.BGP4MP.MESSAGE;
import mrt.type.TABLE_DUMP_V2.RIB_AFI;
import providers.Collector;
import providers.RoutingInformationBase;
import utility.BoundedPriorityBlockingQueue;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

public class RRC extends Collector
{
    /** The id that belongs to this RRC. */
    private final String id;

    /** The queue that the RRC is filling with BGPUpdate messages. */
    private final BoundedPriorityBlockingQueue<BGPReport> queue;

     /** The RRC is collecting data from this time onward. */
    private Instant time;

     /** The interval on which BGPUpdates are stored in an archive file in seconds. */
    private final int interval;

     /** Collection of BGPReaders, such that each reader can have its own settings. */
    private HashMap<String, BGPReader.Settings> peers = new HashMap<>();

    /**  */
    private boolean ENABLE_RIB = false;

    /**
     *
     * @param id The id of the RRC
     * @param queue The queue which should be filled with BGPUpdate messages.
     * @param time The timestamp at which the RRC should start collecting MRT messages.
     */
    public RRC(String id, BoundedPriorityBlockingQueue<BGPReport> queue, Instant time, int interval, boolean ENABLE_RIB)
    {
        this.id = id;
        this.queue = queue;
        this.time = time;
        this.interval = interval;
        this.ENABLE_RIB = ENABLE_RIB;
    }

    /**
     * Stream BGPUpdate messages from a certain point in time.
     */
    @Override
    public void run()
    {
        // Construct the DateFormats that are used to construct the right URL.
        DateTimeFormatter x = DateTimeFormatter.ofPattern("yyyy.MM").withZone(ZoneOffset.UTC);
        DateTimeFormatter y = DateTimeFormatter.ofPattern("yyyyMMdd.HHmm").withZone(ZoneOffset.UTC);

        try
        {
            /* -----------------------------------------
             * Retrieve the Routing Information Base
             * -----------------------------------------*/
            if(ENABLE_RIB)
            {
                // Construct the URL which points to a MRT formatted file with BGPUpdates.
                String url = "http://data.ris.ripe.net/" + id + "/" + x.format(time) + "/bview." + y.format(time.truncatedTo(ChronoUnit.DAYS)) + ".gz";
                System.out.println("Scraping RIB: " + url);

                DataInputStream stream = new DataInputStream(new GZIPInputStream(new URL(url).openStream(), 50000000));
                try
                {
                    MRTReader reader = new MRTReader(stream);
                    while (stream.available() > 0)
                    {
                        MRTRecord entry = reader.read();

                        if ((entry instanceof RIB_AFI))
                            rib.insert((RIB_AFI) entry);
                    }
                } catch (EOFException e)
                {
                    System.out.println("[" + new Date().toString() + "] End of stream found!");
                    stream.close();

                    System.out.println("Routing Information Base successfully parsed.");
                    System.out.println("Routes: " + RoutingInformationBase.counter);
                    System.out.println("Size: " + RoutingInformationBase.total);
                    System.out.println("Average: " +  RoutingInformationBase.total / RoutingInformationBase.counter);
                }

                // Close the stream to free resources.
                stream.close();
            }

            while(true)
            {
                // Construct the URL which points to a MRT formatted file with BGPUpdates.
                String url = "http://data.ris.ripe.net/" + id + "/" + x.format(time) + "/updates." + y.format(time) + ".gz";
                System.out.println("Scraping a new URL: " + url);

                /*
                 * TODO: This is done to prevent: java.net.SocketException: Connection reset
                 */
                File tmp = File.createTempFile("updates", ".tmp");
                FileOutputStream output = new FileOutputStream(tmp);
                output.getChannel().transferFrom(Channels.newChannel(new URL(url).openStream()), 0, Long.MAX_VALUE);
                DataInputStream stream = new DataInputStream(new GZIPInputStream(new FileInputStream(tmp)));

                try
                {
                    MRTReader reader = new MRTReader(stream);
                    while (stream.available() > 0)
                    {
                        MRTRecord entry = reader.read();

                        if (!(entry instanceof MESSAGE))
                            continue;

                        MESSAGE record = (MESSAGE) entry;
                        if(!peers.containsKey(record.peer.getHostAddress()))
                        {
                            // The general settings used to parse the BGP messages.
                            BGPReader.Settings settings = new BGPReader.Settings();
                            settings.EXTENDED_AS4 = true;
                            settings.MRTMode = false;
                            peers.put(record.peer.getHostAddress(), settings);
                        }

                        BGPReader.Settings settings = peers.get(record.peer.getHostAddress());
                        BGPReader parser = new BGPReader(new DataInputStream(new ByteArrayInputStream(record.message)), settings);

                        BGPMessage message;
                        try
                        {
                            message = parser.read();
                        }
                        catch(Exception exception)
                        {
                            System.out.println("First parsing approach failed, changing EXTENDED_AS4 to false for peer: " + record.peer.getHostAddress());
                            settings.EXTENDED_AS4 = false;
                            parser = new BGPReader(new DataInputStream(new ByteArrayInputStream(record.message)), settings);
                            message = parser.read();
                        }

                        if(message instanceof BGPUpdate)
                        {
                            record.message = null;
                            BGPReport report = new BGPReport((BGPUpdate)message, record, this);
                            queue.put(report);
                        }
                    }
                }
                catch (EOFException e)
                {
                    System.out.println("[" + new Date().toString() + "] End of stream found!");
                    stream.close();
                }

                // Update the calendar with 5 minutes, in order to scrape the next message.
                time = time.plus(interval, ChronoUnit.SECONDS);
                tmp.delete();
            }
        } catch (Exception e)
        {
            System.out.println("An exception has occurred: ");
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
