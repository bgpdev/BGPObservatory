package core;

import bgp.BGPUpdate;
import com.google.gson.JsonObject;
import mrt.MRTRecord;
import mrt.type.BGP4MP.MESSAGE;
import providers.Collector;

import java.util.List;
import java.util.concurrent.Future;

/**
 * A BGPReport object holds the analyzers that are currently busy analyzing the update.
 */
public class BGPReport
{
    public final BGPUpdate update;
    public final MESSAGE record;
    public final Collector collector;
    public List<Future<JsonObject>> analyzers;

    public BGPReport(BGPUpdate update, MESSAGE record, Collector collector)
    {
        this.update = update;
        this.record = record;
        this.collector = collector;
    }
}
