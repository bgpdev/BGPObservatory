package core;

import analyzers.*;
import analyzers.chain.ChainedAnalyzer;
import bgp.attributes.AS_PATH;
import bgp.utility.BGPUtility;
import rpsl.RPSLPolicy;
import system.threads.ThreadEngine;
import utility.BoundedPriorityBlockingQueue;

import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;

/**
 * The AnalysisEngine runs a variety of Analyzer subclasses in parallel on each BGPUpdate message.
 */
public class AnalysisEngine implements Runnable
{
    /** The BlockingQueue of which BGPUpdate messages are retrieved. */
    private BlockingQueue<BGPReport> source;

    /** The BlockingQueue to which BGPUpdate messages are written after analysis. */
    private BlockingQueue<BGPReport> destination;

    /** Performance statistics */
    private long counter = 0;
    private long start = System.currentTimeMillis();

    AnalysisEngine(BoundedPriorityBlockingQueue<BGPReport> source, BlockingQueue<BGPReport> destination) throws Exception
    {
        this.source = source;
        this.destination = destination;
    }

    @Override
    public void run()
    {
        while(true)
        {
            try
            {
                BGPReport report = source.take();

                // TODO: Construct list, do this more generic
                ArrayList<Analyzer> analyzers = new ArrayList<>();

                /* ----------------------------------------
                 * Chained Analyzer for Route Leak Hijacks
                 * ----------------------------------------*/
//                {
//                    BGPUtility.normalize(report.update);
//
//                    // TODO: Check if this is necessary
//                    // if(report.update.pathattributes.containsKey("AS_PATH"))
//                    //      BGPUtility.sanitize((AS_PATH) report.update.pathattributes.get("AS_PATH"));
//
//                    ChainedAnalyzer chain = new ChainedAnalyzer();
//                    chain.add(new RouteLeakAnalyzer(), RouteLeakAnalyzer::pass);
//                    //chain.add(new RPSLPolicyAnalyzer(), RPSLPolicyAnalyzer::pass);
//                    chain.setTarget(report);
//                    analyzers.add(chain);
//                }

                /* ----------------------------------------
                 * Chained Analyzer for BGP Origin Hijacks.
                 * ----------------------------------------*/
                {
                    BGPUtility.normalize(report.update);
                    ChainedAnalyzer chain = new ChainedAnalyzer();
                    chain.add(new MOASAnalyzer(), MOASAnalyzer::pass);
                    chain.add(new WHOISAnalyzer(), WHOISAnalyzer::pass);
                    chain.add(new RSEFAnalyzer(), RSEFAnalyzer::pass);
                    chain.add(new RPKIAnalyzer(), RPKIAnalyzer::pass);
                    chain.add(new LACNICAnalyzer(), LACNICAnalyzer::pass);
                    chain.add(new RIPEHistoryAnalyzer(), RIPEHistoryAnalyzer::pass);
                    chain.setTarget(report);
                    analyzers.add(chain);
                }

                report.analyzers = ThreadEngine.getPool().invokeAll(analyzers);
                destination.put(report);

                if(counter++ % 2000 == 0)
                {
                    System.out.println("------------------------------------");
                    System.out.println("BGP Updates processed: " + counter);
                    System.out.println("Per second: " + (double)counter / (double)((System.currentTimeMillis() - start) / 1000));
                    System.out.println("------------------------------------");

                    Core.profiler.print();
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }
}
