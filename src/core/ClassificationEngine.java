package core;

import bgp.utility.BGPUtility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import network.entities.Prefix;
import utility.JSONMerger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;

/**
 * The Classification Engine classifies each BGPUpdate (on per prefix basis).
 * TODO:
 * - Location of the reports should still be specified.
 *
 */
public class ClassificationEngine implements Runnable
{
    /** The Queue from which BGPReports (generated by the AnalyzerEngine) are retrieved. */
    private BlockingQueue<BGPReport> source;

    /**
     * Constructs a ClassificationEngine that takes BGPReports from the Queue
     * and classifies each prefix as a valid update, hijack, route leak, etc...
     * @param queue The BlockingQueue from which BGPReports are received.
     */
    ClassificationEngine(BlockingQueue<BGPReport> queue)
    {
        this.source = queue;
    }

    /**
     * Execute the engine. It keeps waiting for input.
     */
    @Override
    public void run()
    {
        while (true)
            try
            {
                // Take the next BGPReport from the Queue.
                BGPReport report = source.take();

                // Merge all Reports together.
                JsonObject indicators = new JsonObject();
                indicators.add("prefixes", new JsonObject());

                // Merge the reports together.
                // TODO: Room for Improvement.
                for (Future<JsonObject> x : report.analyzers)
                    JSONMerger.extend(indicators, JSONMerger.ConflictStrategy.PREFER_SECOND_OBJ, x.get());

                // Add the prefix as key to the prefix object.
                for(Map.Entry<String, JsonElement> prefix : indicators.get("prefixes").getAsJsonObject().entrySet())
                {
                    prefix.getValue().getAsJsonObject().addProperty("prefix", prefix.getKey());
                    prefix.getValue().getAsJsonObject().addProperty("timestamp", report.record.header.timestamp.toString());
                }

                /* --------------------------------------------
                 * Classify the BGPReports.
                 * --------------------------------------------*/
//                // Validate if a BGPUpdate is not a result of a route leak.
//                classifyRouteLeak(report, indicators);

                // Classify the BGPUpdate message based on indicators
                classifyHijack(report, indicators);

                /* --------------------------------------------
                 * Explicit and Implicit Withdrawals
                 * Update the Routing Information Base
                 * --------------------------------------------*/
                for(Prefix prefix : report.update.withdrawnRoutes)
                    report.collector.rib.withdrawn(prefix.toString(), (long)report.record.peerASN);

                for(Prefix prefix : report.update.nlri)
                    report.collector.rib.update(prefix.toString(),
                            (long)report.record.peerASN,
                            report.record.header.timestamp.getEpochSecond(),
                            report.update.pathattributes);
            }
            catch(Exception e)
            {
                e.printStackTrace();
                System.exit(-1);
            }
    }

    /**
     * Determines if the BGPUpdate message is a result of a ROUTE Leak.
     * @param report The BGPReport which contains the BGPUpdate message.
     * @param indicators The indicators associated to this prefix.
     */
    private void classifyRouteLeak(BGPReport report, JsonObject indicators) throws IOException
    {
        for(Map.Entry<String, JsonElement> prefix_obj : indicators.get("prefixes").getAsJsonObject().entrySet())
        {
            JsonObject properties = prefix_obj.getValue().getAsJsonObject();

            /* -----------------------------------------------------
             * If no route leaks are detected by ASRank, continue.
             * -----------------------------------------------------*/
            if(!properties.has("asrank-route-leaks"))
                continue;

            for (JsonElement leak : properties.get("asrank-route-leaks").getAsJsonArray())
            {
                String offender = "AS" + leak.getAsJsonObject().get("offender").getAsString();
                String to = "AS" + leak.getAsJsonObject().get("to").getAsString();

                String prefix = properties.get("prefix").getAsString()
                        .replaceAll("/", "-")
                        .replaceAll(":", "_");

                System.out.println("Creating a new Route Leak case... " + offender + " " + prefix);
                writeReport("cases/route-leaks/" + offender + "/" + prefix + "/", report, properties);
            }
        }
    }

    /**
     * Inspects a BGPReport and associated indicators to search for BGP Hijacks.
     * @param report BGPReport containing the BGPUpdate message.
     * @param indicators The Indicators generated from the BGPUpdate message.
     * @throws IOException
     */
    private void classifyHijack(BGPReport report, JsonObject indicators) throws IOException
    {
        for(Map.Entry<String, JsonElement> x : indicators.get("prefixes").getAsJsonObject().entrySet())
        {
            JsonObject properties = x.getValue().getAsJsonObject();

            boolean whois = false;
            boolean rpki = false;
            boolean SRH_PO = true;
            boolean SRH_ROO = true;
            boolean soas = true;
            boolean moas = true;

            if(properties.has("introduces-soas"))
                soas = properties.getAsJsonObject().get("introduces-soas").getAsBoolean();
            if(properties.has("introduces-moas"))
                moas = properties.getAsJsonObject().get("introduces-moas").getAsBoolean();

            if(properties.has("whois-match"))
                whois = true;

            if(properties.has("rpki-status"))
                rpki = properties.getAsJsonObject().get("rpki-status").equals("valid");

            if(properties.has("same-resource-holder (prefix-origin)"))
                SRH_PO = properties.getAsJsonObject().get("same-resource-holder (prefix-origin)").getAsBoolean();
            if(properties.has("same-resource-holder (RO-origin)"))
                SRH_ROO = properties.getAsJsonObject().get("same-resource-holder (RO-origin)").getAsBoolean();


            if((soas || moas) && (!whois && !rpki && !SRH_PO && !SRH_ROO))
            {
                System.out.println("Creating new Hijack case..." + x.getValue().getAsJsonObject().get("prefix").getAsString());

                // Retrieve the Origin of the AS_PATH
                long origin = BGPUtility.getOriginatingAS(BGPUtility.getAS_PATH(report.update));

                // TODO: Path should be customizable.
                String prefix = properties.get("prefix").getAsString()
                        .replaceAll("/", "-")
                        .replaceAll(":", "_");

                if(properties.get("announced-earlier (exact)").getAsBoolean())
                    writeReport("cases/hijacks/announced-exact/" + origin + "/" + prefix + "/", report, properties);
                else if(properties.get("announced-earlier (parent)").getAsBoolean())
                    writeReport("cases/hijacks/announced-parent/" + origin + "/" + prefix + "/", report, properties);
                else
                    writeReport("cases/hijacks/hijacks/" + origin + "/" + prefix + "/", report, properties);
            }
        }
    }

    /**
     *
     * @param path The path where the report should be stored.
     * @param report The BGPReport containing the BGPUpdate message.
     * @param indicators The list of indicators as generated by the Analyzers.
     */
    private void writeReport(String path, BGPReport report, JsonObject indicators) throws IOException
    {
        // Register a Prefix adapter for Prefix.
        GsonBuilder builder = new GsonBuilder().setPrettyPrinting();
        builder.registerTypeAdapter(Prefix.class, new Prefix.Serializer());
        Gson gson = builder.create();

        indicators.getAsJsonObject().add("update-message", gson.toJsonTree(report.update));

        File file = new File(path + "/" + report.record.header.timestamp.getEpochSecond());
        if (!file.exists())
        {
            // Create the file and potential parent directories.
            file.getParentFile().mkdirs();

            if(!file.createNewFile())
                throw new IOException("Could not create a new file.");

            // Write the indicators to disk.
            FileOutputStream stream = new FileOutputStream(file, false);
            stream.write(gson.toJson(indicators).getBytes());
        }
    }
}