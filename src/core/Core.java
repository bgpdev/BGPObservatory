package core;

import irr.IRR;
import irr.RPKI;
import irr.RoutingRegistry;
import po.PO;
import rsef.RSEF;
import rsef.RSEFRecord;
import collections.trees.BinaryTree;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * The Core class contains all the global settings and provides access to global resources.
 * TODO: Remove old entries in the HashMap
 */
public class Core
{
    public static final boolean ENABLE_RIB = true;

    public static Profiler profiler = new Profiler();

    private static HashMap<Instant, Network> networks = new HashMap<>();
    private static HashMap<Instant, IRR> registries = new HashMap<>();
    private static HashMap<Instant, BinaryTree<ArrayList<RPKI.ROA>>> rpki = new HashMap<>();
    private static HashMap<Instant, BinaryTree<ArrayList<PO>>> registry_trees = new HashMap<>();

    /** RSEF Related data sources */
    private static HashMap<Instant, List<RSEFRecord>> rsef_records = new HashMap<>();

    public synchronized static Network getNetwork(Instant time)
    {
        try
        {
            Instant truncated = time.truncatedTo(ChronoUnit.DAYS);
            if (!networks.containsKey(truncated))
                networks.put(truncated, new Network(truncated));
            return networks.get(truncated);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }

    public synchronized static IRR getIRR(Instant time)
    {
        Instant truncated = time.truncatedTo(ChronoUnit.DAYS);
        if (!registries.containsKey(truncated))
            registries.put(truncated, new IRR(truncated));
        return registries.get(truncated);
    }

    public synchronized static BinaryTree<ArrayList<PO>> getRegistryTree(Instant time)
    {
        try
        {
            Instant truncated = time.truncatedTo(ChronoUnit.DAYS);
            if (!registry_trees.containsKey(truncated))
                registry_trees.put(truncated, RoutingRegistry.getRegistryTree(new Date(truncated.toEpochMilli())));
            return registry_trees.get(truncated);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }

    public synchronized static List<RSEFRecord> getRSEF(Instant time)
    {
        try
        {
            Instant truncated = time.truncatedTo(ChronoUnit.DAYS);
            if (!rsef_records.containsKey(truncated))
                rsef_records.put(truncated, RSEF.getRecords(truncated));
            return rsef_records.get(truncated);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }

    public synchronized static BinaryTree<ArrayList<RPKI.ROA>> getRPKI(Instant time)
    {
        try
        {
            Instant truncated = time.truncatedTo(ChronoUnit.DAYS);
            if (!rpki.containsKey(truncated))
                rpki.put(truncated, RPKI.getRPKITree(time));
            return rpki.get(truncated);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(-1);
            return null;
        }
    }
}
