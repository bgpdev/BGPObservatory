package core;

import providers.ripe.RRC;
import utility.BoundedPriorityBlockingQueue;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * The main class that contains all the
 */
public class Observatory
{
    /**
     * The main entry point of the BGP Observatory.
     * @param argv Any arguments
     * @throws Exception Any exceptions that might occur.
     */
    public static void main(String argv[]) throws Exception
    {
        /* -----------------------------------------------------------------------------------------
         * Set time that the Observatory should start reading BGPUpdates.
         * -----------------------------------------------------------------------------------------*/
        Instant time = Instant.parse("2018-09-20T06:00:00.00Z");
        System.out.println("Starting to parse at: " + time);

        /* -----------------------------------------------------------------------------------------
         * Construct the global Queues that are used to pass messages.
         * Note: The messages are ordered per timestamp.
         * -----------------------------------------------------------------------------------------*/
        BoundedPriorityBlockingQueue<BGPReport> update_queue = new BoundedPriorityBlockingQueue<>(50,
                Comparator.comparing(a -> a.record.header.timestamp));

        BlockingQueue<BGPReport> report_queue = new ArrayBlockingQueue<>(50);

        /* -----------------------------------------------------------------------------------------
         * Retrieve all the RIPE RRC that we want to parse from.
         * -----------------------------------------------------------------------------------------*/
        ArrayList<RRC> collectors = new ArrayList<>();
        for(String rrc : "1".split(" "))
        {
            if(rrc.length() == 1)
                collectors.add(new RRC("rrc0" + rrc, update_queue, time, 300, Core.ENABLE_RIB));
            else
                collectors.add(new RRC("rrc" + rrc, update_queue, time, 300, Core.ENABLE_RIB));
        }

        /* -----------------------------------------------------------------------------------------
         * Stage #3: Start the ClassificationEngine and process the BGPUpdate messages.
         *  -----------------------------------------------------------------------------------------*/
        ClassificationEngine classificationEngine = new ClassificationEngine(report_queue);
        new Thread(classificationEngine).start();

        /* -----------------------------------------------------------------------------------------
         * Stage #2: Start the AnalysisEngine and process the BGPUpdate messages.
         *  -----------------------------------------------------------------------------------------*/
        AnalysisEngine analysisEngine = new AnalysisEngine(update_queue, report_queue);
        new Thread(analysisEngine).start();

        /* -----------------------------------------------------------------------------------------
         *  Stage #1: Process RIPE RRC update messages and store them in a queue.
         * -----------------------------------------------------------------------------------------*/
        for(RRC collector : collectors)
            new Thread(collector).start();
    }
}
