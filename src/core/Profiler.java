package core;

import javax.sound.midi.SysexMessage;
import java.util.HashMap;
import java.util.Map;

public class Profiler
{
    private class Measurement
    {
        long measurements = 0;
        long duration = 0;
    }

    /** The map of ongoing times that need to be closed.*/
    private Map<String, Measurement> measurements = new HashMap<>();
    private long start = System.nanoTime();

    public synchronized long start()
    {
        return System.nanoTime();
    }

    public synchronized void stop(long start, String type)
    {
        Long end = System.nanoTime();

        if(!measurements.containsKey(type))
            measurements.put(type, new Measurement());

        Measurement measurement = measurements.get(type);
        measurement.measurements++;
        measurement.duration += (end - start);
    }

    public synchronized void print()
    {
        long now = System.nanoTime();

        System.out.println("---------------------------------------");
        System.out.println("Profiler Report");

        for(Map.Entry<String, Measurement> x : measurements.entrySet())
        {
            System.out.println(x.getKey() + " Measurements: " + x.getValue().measurements);
            System.out.println(x.getKey() + " Total Duration: " + x.getValue().duration);
            System.out.println(x.getKey() + " Per Measurement: " + ((double)x.getValue().duration / (double)x.getValue().measurements) + " nanoseconds");
            System.out.println(x.getKey() + " % of all: " + ((double)x.getValue().duration / (double)(now - start)) + "%");
            System.out.println();
        }
        System.out.println("---------------------------------------");
    }
}
